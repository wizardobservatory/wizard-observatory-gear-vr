﻿using UnityEngine;
using System.Collections;

public class BulletDestroyer : MonoBehaviour {

	private float time = 0;
	public float destroyTime;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		if (time > destroyTime)
			Destroy (gameObject);
	}
}
