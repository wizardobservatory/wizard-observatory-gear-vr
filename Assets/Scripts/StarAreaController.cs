﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StarAreaController : MonoBehaviour {

	private LineRenderer lineRenderer;
	public List<GameObject> stars;
	public int currentSelection = -1;
	public bool powerUpKillAll = false;
	public bool powerUpAutoWeapon = false;
	public GameObject audioSourceGO;

	// Use this for initialization
	void Start () {
		lineRenderer = GetComponent<LineRenderer> ();
		Debug.Log ("Outaaa");
	}
	
	// Update is called once per frame
	void Update () {
		print ("Outaaa");
		//Debug.DrawRay (Camera.main.transform.position, UnityEngine.VR.InputTracking.GetLocalRotation (UnityEngine.VR.VRNode.Head) * Vector3.forward, Color.red);
		//Ray ray = new Ray (UnityEngine.VR.InputTracking.GetLocalPosition (UnityEngine.VR.VRNode.Head), UnityEngine.VR.InputTracking.GetLocalRotation(UnityEngine.VR.VRNode.Head)*Vector3.forward);
		Ray ray = new Ray (Camera.main.transform.position, UnityEngine.VR.InputTracking.GetLocalRotation(UnityEngine.VR.VRNode.Head)*Vector3.forward);
		Debug.Log("Rotation Y: " + Camera.main.transform.rotation.y);
		//Ray ray = new Ray (Camera.main.transform.position, Camera.main.transform.rotation*Vector3.forward);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit) && hit.collider.tag == "Stars") {
			if (hit.collider.gameObject == stars[currentSelection+1]) {
				currentSelection++;
				lineRenderer.SetVertexCount (currentSelection + 2);
				lineRenderer.SetPosition (currentSelection, hit.collider.transform.position);
			}
			lineRenderer.SetPosition (currentSelection + 1, hit.point);
			if (currentSelection+1 == stars.Count) {
				if (powerUpKillAll) {
					audioSourceGO.GetComponent<AudioSource> ().Play ();
					GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
					for(int i = 0; i < enemies.Length; i++) {
						Destroy (enemies [i]);
					}
				}
				if (powerUpAutoWeapon) {
					if (VRShootingInputController.autoFreq == 1)
						VRShootingInputController.autoFreq = 2;
					else if(VRShootingInputController.autoFreq < 8)
						VRShootingInputController.autoFreq = VRShootingInputController.autoFreq * VRShootingInputController.autoFreq;
					VRShootingInputController.autoInterval = -1;
					VRShootingInputController.autoShoot = true;
				}
				lineRenderer.SetVertexCount (0);
				currentSelection = -1;
			}
			Debug.Log ("Out");
		} else {
			lineRenderer.SetVertexCount (0);
			currentSelection = -1;
		}
	}
}
