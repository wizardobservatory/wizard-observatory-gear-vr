﻿using UnityEngine;
using System.Collections;


namespace VRStandardAssets.Utils{
	public class Des : MonoBehaviour {

		//[SerializeField] private VRInteractiveItem m_InteractiveItem;
		//[SerializeField] private VRInput m_VRInput;
		//    public Vector3 three60= new Vector3(0,180,0);
		public GameManager m_GameManager;
		public Transform target;
		private int speed=5;


		private void OnEnable(){
			//    m_InteractiveItem.OnClick += HandleClick;
		}

		private void OnDisable(){
			//    m_InteractiveItem.OnClick -=HandleClick;
		}

		private void HandleClick(){
			Destroy (gameObject);
		}

		// Use this for initialization
		void Start () {
			m_GameManager = GameObject.Find ("SC Holder").GetComponent<GameManager> ();
			target = GameObject.Find ("Target").transform;
			transform.LookAt (target);
		//s	transform.Rotate (0, 0, 0);
		}

		// Update is called once per frame
		void Update () {
			transform.LookAt (target);
			this.transform.Translate(Vector3.forward * Time.deltaTime*speed);

		}

		void OnTriggerEnter (Collider collider){
			Destroy(gameObject);

		}
	}
}
