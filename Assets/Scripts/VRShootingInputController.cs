﻿using UnityEngine;
using System.Collections;

public class VRShootingInputController : MonoBehaviour {

	public Rigidbody projectilePrefab;
	public float projectileSpeed;
	public Vector3 rotation;
	public static bool autoShoot = false;
	public static float autoFreq = 1;
	public static float autoInterval = -1;
	private float timeCounter = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (autoShoot && autoInterval == -1) {
			autoInterval = 1f / autoFreq;
		}
		if (Input.GetMouseButtonDown (0)) {
//		if (true) {
			if (!autoShoot) {
				Shoot ();
				return;
			}
		}
		if (Input.GetMouseButton (0) && autoShoot){
			timeCounter += Time.deltaTime;
			if (timeCounter >= autoInterval) {
				Shoot ();
				timeCounter = 0;
			}
		}
	}

	void Shoot(){
		Quaternion VRdir = UnityEngine.VR.InputTracking.GetLocalRotation(UnityEngine.VR.VRNode.Head);
		Quaternion direction = transform.rotation;

		//			Quaternion projectileRotation = Quaternion.Euler (135 + VRdir.x, 45 + VRdir.y, 0);

		//Ray ray = Camera.main.ScreenPointToRay (Vector3.zero);
		Ray ray = new Ray (Camera.main.transform.position, UnityEngine.VR.InputTracking.GetLocalRotation(UnityEngine.VR.VRNode.Head)*Vector3.forward);

		RaycastHit hit;
		Physics.Raycast (ray, out hit);

		Rigidbody projectile = (Rigidbody) Instantiate (projectilePrefab, transform.position, Quaternion.identity);
		projectile.velocity = VRdir*Vector3.forward * projectileSpeed;
		projectile.transform.LookAt (hit.point);
		projectile.transform.Rotate (rotation);
		//projectile.transform.rotation = Quaternion.Euler (0, 45, 0) * projectile.transform.rotation;
		GetComponent<ShootingAudioController>().PlaySting();
	}


}
