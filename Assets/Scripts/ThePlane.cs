﻿using System.Collections.Generic;
using System.Security.Policy;
using System.Threading;
using UnityEngine;

public class ThePlane : MonoBehaviour
{
    //public LineRenderer Linerenderer;
    private Vector3 _startPos;
    private Vector3 _endPos;
    public List<GameObject> Objects;
    public GameObject Prefab;
    private int i = 0;
    private bool selected = false;
    public LineRenderer Linerenderer;

    void Start()
    {
        Linerenderer.SetVertexCount(0);
    }

    void Update()
	{
		RaycastHit seen;
		Ray raydirection = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
		if (Physics.Raycast(raydirection, out seen, 50f))
		{
			if (seen.collider.tag == "Stars")
			{
				seen.collider.transform.position = new Vector3 (0, 20, 0);
			}

		}


		_startPos = Input.mousePosition;
		_startPos.z = Camera.main.nearClipPlane;
		Ray ray = Camera.main.ScreenPointToRay(_startPos);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit) && i < Objects.Count) {
			if (Objects [i] == hit.collider.gameObject) {
				if (i > 0 && i < Objects.Count) {
					GameObject a = Instantiate (Prefab);
					LineRenderer b = a.AddComponent<LineRenderer> ();
					b.SetWidth (0.05f, 0.05f);
					b.SetPosition (0, Objects [i - 1].transform.position);
					b.SetPosition (1, Objects [i].transform.position);
					Linerenderer.SetPosition (0, Objects [i].transform.position);
				}
				i++;
				if (i >= Objects.Count)
					Linerenderer.SetVertexCount (0);
			}
			Linerenderer.SetPosition (1, hit.point);
		} else if (i < Objects.Count) {

			//Linerenderer.SetPosition (1, Camera.main.ScreenToWorldPoint (_startPos));
		}
    }
}
