﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class StarController : MonoBehaviour
{
    public LineRenderer Linerenderer;
    private Vector3 _startPos;
    private Vector3 _endPos;
    private bool _mousebutton;
    public List<GameObject> Objects;
    public GameObject Prefab;
    private GameObject prev;
    private GameObject now;
    private int i = 0;
    private bool selected = false;
    // Use this for initiali.zation
    void Start()
    {
        _mousebutton = false;
    }

    void Update()
    {
        _startPos = Input.mousePosition;
        _startPos.z = Camera.main.nearClipPlane;
        Ray ray = Camera.main.ScreenPointToRay(_startPos);
        RaycastHit hit;
		if (Physics.Raycast (ray, out hit) && i < Objects.Count) {
			if (Objects [i] == hit.collider.gameObject) {
				if (i > 0 && i < Objects.Count) {
					GameObject a = Instantiate (Prefab);
					LineRenderer b = a.AddComponent<LineRenderer> ();
					b.SetWidth (0.05f, 0.05f);
					b.SetPosition (0, Objects [i - 1].transform.position);
					b.SetPosition (1, Objects [i].transform.position);
					Linerenderer.SetPosition (0, Objects [i].transform.position);
				}
				i++;
				if (i >= Objects.Count)
					Linerenderer.SetVertexCount (0);
			}
		} else if (i < Objects.Count) {

			Linerenderer.SetPosition (1, Camera.main.ScreenToWorldPoint (_startPos));
		}
        
/*if (Input.GetMouseButtonDown(0))
        {
            LinerendererSetVertexCount(2);
            _startPos = Input.mousePosition;
            _startPos.z = Camera.main.nearClipPlane;
            Ray ray = Camera.main.ScreenPointToRay(_startPos);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (Objects.Count > i && Objects[i] == hit.collider.gameObject)
                {
                    now = Objects[i];
                    i++;
                }
            }
            Linerenderer.SetPosition(0, Camera.main.ScreenToWorldPoint(_startPos));
            _mousebutton = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Linerenderer.SetVertexCount(0);
            _mousebutton = false;   
        }
        if (_mousebutton)
        {
            _endPos = Input.mousePosition;
            _endPos.z = Camera.main.nearClipPlane;
            Ray ray = Camera.main.ScreenPointToRay(_endPos);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (Physics.Raycast(ray, out hit))
                {
                    if (Objects.Count > i && Objects[i] == hit.collider.gameObject)
                    {
                        prev = now;
                        now = Objects[i];
                        Debug.Log(i + " " + (i-1) + " " + Camera.main.ScreenToWorldPoint(_startPos));
                        GameObject a = Instantiate(Prefab);
                        LineRenderer b = a.AddComponent<LineRenderer>();
                        b.SetWidth(0.05f,0.05f);
                        b.SetPosition(0, Objects[i - 1].transform.position);
                        b.SetPosition(1, Objects[i].transform.position);
                        i++;
                    }
                    Linerenderer.SetPosition(1, Camera.main.ScreenToWorldPoint(_endPos));
                }
            }
        }*/
    }
}
