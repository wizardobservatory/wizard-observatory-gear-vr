﻿using UnityEngine;
using System.Collections;

public class Worm : MonoBehaviour {

	private Transform target1;
	private Transform me1;
	private int rnd=16;

	// Use this for initialization
	void Start () {
		target1 = GameObject.Find ("AITargetDummy").transform;
		//transform.LookAt (tower1);
		me1 = this.transform;
		me1.LookAt (target1);
	}

	// Update is called once per frame
	void Update () {

		me1.Translate (Vector3.forward * Time.deltaTime *2f);


		transform.Rotate (Vector3.right, Time.deltaTime*1000f);
		Vector3 newPos = Vector3.MoveTowards (transform.position, target1.transform.position, 0.07f);
		transform.position = newPos;

		Vector3 current_Pos= (me1.position);
		Vector3 why;
		why.x = current_Pos.x+Random.Range(1,5);
		why.y = current_Pos.y;
		why.z = current_Pos.z+Random.Range(1,5);


		rnd = Random.Range (1, 1000);
		if (rnd == 13) {
			me1.position = why;
			me1.LookAt (target1);
		}
	}

	void OnTriggerEnter (Collider collider){
		Destroy(gameObject);

	}
}
