﻿using UnityEngine;
using System.Collections;

public class EnemyCollisionController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider collider){
		if (collider.tag.Equals ("Bullet")) {
			// Handle death
			GetComponent<ShootingAudioController>().PlaySting();
			Destroy (gameObject);
		}
	}
}
