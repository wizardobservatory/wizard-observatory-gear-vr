﻿using UnityEngine;
using System.Collections;

public class TowerController : MonoBehaviour {

	public int lifes = 10;
	private float yDecrement;
	public float targetY;
	public bool animate = false;
	public float shake = 0;
	public float shakeAmount = 0.7f;
	public float shakeDecrement = 1.0f;
	public float animateSpeed = 1.0F;
	private float startTime;
	private float journeyLength;

	// Use this for initialization
	void Start () {
		targetY = transform.position.y;
		yDecrement = 20f / lifes;
	}
	
	// Update is called once per frame
	void Update () {
		if (animate) {
			//Debug.Log (targetY + " - " + transform.position.y + " - " + (targetY >= transform.position.y));
			if (!(targetY >= transform.position.y)) {
				float distCovered = (Time.time - startTime) * animateSpeed;
				float fracJourney = distCovered / journeyLength;
				transform.position = Vector3.Lerp (transform.position, new Vector3(0, targetY, 0), fracJourney);
			} else {
				transform.position = new Vector3 (0, targetY, 0);
				animate = false;
			}
		}
		if (shake > 0) {
			Camera.main.transform.localPosition = Random.insideUnitSphere * shakeAmount;
			shake -= Time.deltaTime * shakeDecrement;
		} else {
			shake = 0.0f;
		}
	}

	void OnTriggerEnter(Collider collider){
		if (collider.tag.Equals ("Enemy")) {
			Destroy (collider);
			GetComponent<ShootingAudioController> ().PlaySting ();
			if (lifes == 0) {
				// TODO: END GAME
				return;
			}
			lifes--;
			targetY = targetY - yDecrement;
			animate = true;
			shake = 2;
			startTime = Time.time;
			journeyLength = Vector3.Distance (transform.position, new Vector3 (0, targetY, 0));
		}
	}
}
