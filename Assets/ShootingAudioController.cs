﻿using UnityEngine;
using System.Collections;

public class ShootingAudioController : MonoBehaviour {

	public AudioClip[] stings;
	public AudioSource stingSource;

	public void PlaySting()
	{
		int randClip = Random.Range (0, stings.Length);
		stingSource.clip = stings[randClip];
		stingSource.Play();
	}
}
